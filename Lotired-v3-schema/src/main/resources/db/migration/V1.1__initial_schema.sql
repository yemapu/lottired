CREATE TABLE CART
(
    id              UUID,
    product_id      VARCHAR,
    pay_method      VARCHAR,
    session_id      VARCHAR,
    user_id         VARCHAR,
    total_cost      double precision,
    expiration_date TIMESTAMP,
    created_at      TIMESTAMP,
    updated_at      TIMESTAMP,
    CONSTRAINT cart_pk PRIMARY KEY (id)
);
CREATE TABLE CART_ITEM
(
    id             UUID,
    lottery_id     VARCHAR,
    draw_id        VARCHAR,
    number_play    VARCHAR,
    cart_id        UUID,
    value          DOUBLE PRECISION,
    serie_id       VARCHAR,
    fraction      VARCHAR,
    discount      VARCHAR,
    coupon        VARCHAR,
    reservation_id NUMERIC,
    item_quantity  NUMERIC,
    created_at     TIMESTAMP,
    CONSTRAINT cart_item_pk PRIMARY KEY (id),
    CONSTRAINT cart_item_cart_fk FOREIGN KEY (cart_id) REFERENCES CART (id)
);