package com.lottired.v3.app.rest;

import com.lottired.v3.core.service.BalanceMovementService;
import com.lottired.v3.domain.model.BalanceMovementRecord;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api/balance")
@RestController
public class BalanceController {

    private final BalanceMovementService balanceMovementService;

    public BalanceController(BalanceMovementService balanceMovementService) {
        this.balanceMovementService = balanceMovementService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<BalanceMovementRecord> findAllPaging(@RequestParam("page") int page, @RequestParam("size") int size){
        return balanceMovementService.getAllPaging(page, size);
    }
}
