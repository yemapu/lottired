package com.lottired.v3.app.rest;

import com.lottired.v3.common.response.DrawResponse;
import com.lottired.v3.core.service.DrawService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api/draw")
@RestController
public class DrawController {

    @Autowired
    private DrawService drawService;

    @GetMapping
    public List<DrawResponse> getAllDraws(){
        return drawService.getAll();
    }
}
