package com.lottired.v3.app.rest;

import com.lottired.v3.core.service.LotteryService;
import com.lottired.v3.domain.model.IHistoricalResult;
import com.lottired.v3.domain.model.LotteryRecord;
import com.lottired.v3.domain.model.DrawRecord;
import com.lottired.v3.domain.model.LotterySalesRecord;
import com.lottired.v3.domain.repository.LotterySaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/lottery")
public class LotteryController {

    @Autowired
    private LotteryService lotteryService;

    @Autowired
    private LotterySaleRepository lotterySaleRepository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<LotteryRecord> getAll() {
        return lotteryService.getAll();
    }

    @GetMapping("/sorteos")
    public List<DrawRecord> getAllSorteos() {
        return lotteryService.getAllSorteos();
    }

    @GetMapping("/sorteos/status")
    public List<DrawRecord> getAllActiveDraws() {
        return lotteryService.findAllByStatus();
    }

    @GetMapping("/historical")
    public List<IHistoricalResult> getAllHistorical(@RequestParam(value = "lotteryId") String lotteryId, @RequestParam(value = "drawCode") String drawCode) {
        return lotteryService.getAllHistorical(lotteryId, drawCode);
    }

    //TODO EXTRACT THIS LOGIC TO THEIR OWN SERVICE
    @GetMapping("/historical/sales")
    public List<LotterySalesRecord> getAllHistoricalSales(@RequestParam(value = "saleId") long saleId) {
        return lotterySaleRepository.findAllByQuery(saleId);
    }

    @GetMapping("/historical/sales/other")
    public List<LotterySalesRecord> getAllHistoricalSalesOther(@RequestParam(value = "saleId") long saleId, @RequestParam(value = "billId") long billId) {
        return lotterySaleRepository.findAllByQueryOther(saleId, billId);
    }
}
