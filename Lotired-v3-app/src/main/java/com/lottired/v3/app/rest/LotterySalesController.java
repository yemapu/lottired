package com.lottired.v3.app.rest;

import com.lottired.v3.core.service.LotterySalesService;
import com.lottired.v3.domain.model.LotterySalesRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/lottery-sales")
public class LotterySalesController {

    private final LotterySalesService lotterySalesService;

    public LotterySalesController(LotterySalesService lotterySalesService) {
        this.lotterySalesService = lotterySalesService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<LotterySalesRecord> getAllPaging(@RequestParam("page") int page, @RequestParam("size") int size){
        return lotterySalesService.getAllPaging(page, size);
    }
}
