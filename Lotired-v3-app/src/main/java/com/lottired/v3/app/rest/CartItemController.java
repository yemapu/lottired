package com.lottired.v3.app.rest;


import com.lottired.v3.common.request.CreateCartItemRequest;
import com.lottired.v3.core.service.CartItemService;
import com.lottired.v3.domain.model.CartItemRecord;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cart-item")
public class CartItemController {
    public final CartItemService cartItemService;

    public CartItemController(CartItemService cartItemService) {
        this.cartItemService = cartItemService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartItemRecord createItem(@RequestBody CreateCartItemRequest request) throws Exception {
        return cartItemService.createItem(request);
    }
    @DeleteMapping("/{cartItemId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable("cartItemId") String cartItemId){
        cartItemService.deleteById(cartItemId);
    }

    @DeleteMapping("/cart/{cartId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteAllByCartId(@PathVariable("cartId") String cartId){
        cartItemService.deleteAllByCartId(cartId);
    }

}
