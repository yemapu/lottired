package com.lottired.v3.app.rest;

import com.lottired.v3.common.request.CreateCartRequest;
import com.lottired.v3.core.service.CartService;
import com.lottired.v3.domain.model.CartRecord;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cart")
public class CartController {

    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CartRecord> getAll(){
        return cartService.getAll();
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartRecord addToCart(@RequestBody CreateCartRequest request){
        return cartService.addToCart(request);
    }

    @DeleteMapping("/{cartId}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("cartId") String cartId){
        cartService.deleteById(cartId);
    }


}
