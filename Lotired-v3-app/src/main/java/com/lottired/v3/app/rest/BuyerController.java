package com.lottired.v3.app.rest;

import com.lottired.v3.common.response.BuyerResponse;
import com.lottired.v3.core.service.BuyerService;
import com.lottired.v3.domain.model.BuyerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/buyer")
public class BuyerController {

    private final BuyerService buyerService;

    public BuyerController(BuyerService buyerService) {
        this.buyerService = buyerService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<BuyerResponse> getAll(@RequestParam("page") int page, @RequestParam("size") int size){
        return buyerService.getAll(page, size);
    }
}
