package com.lottired.v3.app.rest;

import com.lottired.v3.common.response.PrizePlanResponse;
import com.lottired.v3.core.service.PrizePlanService;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/prize-plan")
public class PrizePlanController {

    private final PrizePlanService prizePlanService;

    public PrizePlanController(PrizePlanService prizePlanService) {
        this.prizePlanService = prizePlanService;
    }

    @GetMapping
    public List<PrizePlanResponse> getAll(@Param("lottery") String lottery, @Param("draw") String draw){
        return prizePlanService.getAllByQuery(lottery,draw);
    }
}
