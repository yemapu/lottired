package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.PointRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointRepository extends JpaRepository<PointRecord,Long> {
}
