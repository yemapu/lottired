package com.lottired.v3.domain.model;

import com.lottired.v3.domain.pk.DrawPK;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "SORTEOS", schema = "lottery_schema")
public class DrawRecord {

    @EmbeddedId
    private DrawPK drawPK;
    /**
     * Fecha en que juega el sorteo
     */
    @Column(name = "FECHASORTEO")
    private LocalDateTime startAt;

    @Column(name = "DIASORTEO")
    private Integer gameDay;
    /**
     * Fecha en que abre el sorteo
     */
    @Column(name = "HORASORTEO")
    private LocalDateTime gameStartHour;

    @Column(name = "TOTALFRACCIONES")
    private Integer totalFractions;

    @Column(name = "VALORPREMIOMAYOR")
    private Double greatestPrizeValue;

    @Column(name = "VALORBILLETE")
    private String ticketValue;

    @Column(name = "VALORFRACCION")
    private Double ticketFractionValue;

    @Column(name = "HORADEVOLUCIONVENDEDOR")
    private LocalDateTime timeReturnSeller;

    @Column(name = "HORADEVOLUCIONCANAL")
    private LocalDateTime timeReturnCanal;

    @Column(name = "HORADEVOLUCIONLOTERIA")
    private LocalDateTime devolutionRaffle;
    /**
     * Hora en que abre el sorteo
     */
    @Column(name = "HORAAPERTURA", columnDefinition = "TIMESTAMP")
    private LocalDateTime openDate;
    /**
     * Hora de cierre del sorteo
     */
    @Column(name = "HORACIERRE", columnDefinition = "TIMESTAMP")
    private LocalDateTime closeHour;

    @Column(name = "VENTAPROGRAMADA")
    private String scheduleSale;

    @Column(name = "ESTADO")
    private String status;

    @Column(name = "JUGANDO")
    private String playing;

    @Column(name = "CADENAPADRE")
    private String primaryString;

    @Column(name = "PLANOPROMOCIONAL")
    private String promotional;

    @Column(name = "DIGITOSANEXOS")
    private Integer digitsAnnexes;

    @Column(name = "NROANEXOS")
    private String nroAnexos;

    @Column(name = "VENTAS")
    private Integer sales;

    @Column(name = "ENTREGA_ANEXO_VENTA")
    private Integer deliveryAnnexSale;

    @Column(name = "DESCRIPCION_NUM_ANEXO")
    private String annexDescription;

    @Column(name = "ENTREGA_PROMO_VENTA")
    private Integer deliveryPromotionSale;

    @Column(name = "CODIGOBILLETE")
    private Integer ticketCode;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "empresa", referencedColumnName = "empresa", insertable = false, updatable = false),
            @JoinColumn(name = "loteria", referencedColumnName = "loteria", insertable = false, updatable = false)
    })
    private LotteryRecord lotteryRecord;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "empresa", referencedColumnName = "empresa", insertable = false, updatable = false),
            @JoinColumn(name = "cadena", referencedColumnName = "cadena", insertable = false, updatable = false),
            @JoinColumn(name = "loteria", referencedColumnName = "loteria", insertable = false, updatable = false)
    })
    private LotteryDistributorRecord lotteryDistributor;
}
