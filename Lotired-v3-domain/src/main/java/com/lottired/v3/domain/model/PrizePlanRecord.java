package com.lottired.v3.domain.model;

import com.lottired.v3.domain.pk.PrizePlanPK;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "PLANDEPREMIOS", schema = "lottery_schema")
public class PrizePlanRecord {

    @EmbeddedId
    private PrizePlanPK id;

    @Column(name = "DESCRIPCION")
    private String description;

    @Column(name = "VALORPREMIO")
    private double prizeValue;

    @Column(name = "TOTALFRACCIONES")
    private int totalFractions;

    @Column(name = "VALORPREMIOFRACCION")
    private double valuePrizeFraction;

    @Column(name = "PROCEDIMIENTO")
    private String process;

    @Column(name = "CANTIDADPREMIOS")
    private int prizeQuantity;

    @Column(name = "APROXIMACION")
    private String approximate;

    @Column(name = "NUMERO")
    private String number;

    @Column(name = "SERIE")
    private String serie;

    @Column(name = "PREMIOPADRE")
    private String primaryPrize;

    @Column(name = "DIGITOSANEXOS")
    private String annexedDigits;

    @Column(name = "VALOR17PREMIOFRACCION")
    private int value17PrizeFraction;

    @Column(name = "NUMEROANEXO")
    private String annexedNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "empresa", referencedColumnName = "empresa", insertable = false, updatable = false),
            @JoinColumn(name = "loteria", referencedColumnName = "loteria", insertable = false, updatable = false),
            @JoinColumn(name = "sorteo", referencedColumnName = "sorteo", insertable = false, updatable = false),
    })
    private DrawRecord draw;


}
