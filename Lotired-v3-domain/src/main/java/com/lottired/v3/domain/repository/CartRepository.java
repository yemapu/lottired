package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.CartRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CartRepository extends JpaRepository <CartRecord,UUID> {

}
