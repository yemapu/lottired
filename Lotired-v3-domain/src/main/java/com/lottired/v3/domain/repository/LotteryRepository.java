package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.LotteryRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LotteryRepository extends JpaRepository<LotteryRecord, Long> {
}
