package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.CartItemRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface CartItemRepository extends JpaRepository <CartItemRecord, UUID>{

    void deleteAllByCartId(UUID cartId);
}
