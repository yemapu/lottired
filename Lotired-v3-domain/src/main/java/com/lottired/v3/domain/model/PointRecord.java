package com.lottired.v3.domain.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "POINT_RECORD", schema = "lottery_schema")
public class PointRecord {

    @Id
    private long id;
    private LocalDateTime date;
    private int points;
    private double value;
    private String transactionType;
    private String transactionStatus;
    private String locationName;
    private String partnerName;
    private String paymentMethod;
    private String referredAccount;
    private String mccDescription;
    private String commercialDescription;
    private String comments;
}
