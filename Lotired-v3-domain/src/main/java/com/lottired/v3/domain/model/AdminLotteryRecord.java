package com.lottired.v3.domain.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "ADMIN_LOTTERY", schema = "lottery_schema")
public class AdminLotteryRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "admin_lottery_generator")
    @SequenceGenerator(name = "admin_lottery_generator", sequenceName = "seq_admin_lottery", allocationSize = 1)
    private long id;

    private String logo;
    private String banner;
    private String backGround;
    private String displayName;

}
