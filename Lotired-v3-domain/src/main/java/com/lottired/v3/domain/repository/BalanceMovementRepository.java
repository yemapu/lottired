package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.BalanceMovementRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BalanceMovementRepository extends JpaRepository<BalanceMovementRecord, Long>{

}
