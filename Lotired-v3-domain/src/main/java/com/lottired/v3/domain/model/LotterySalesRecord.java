package com.lottired.v3.domain.model;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "ventasloteriasportal", schema = "lottery_schema")
public class LotterySalesRecord {

    @Id
    @Column(name = "ID")
    private long id;

    @Column(name = "REFERENCIA")
    private String reference;
    @Column(name = "MONEDA")
    private String currency;
    @Column(name = "VALORTOTAL")
    private Double totalValue;
    @Column(name = "VALORIMPUESTO")
    private Double taxValue;
    @Column(name = "MONEDABANCARIA")
    private String bankCurrency;
    @Column(name = "FACTORCONVERSION")
    private String conversionFactor;
    @Column(name = "NOMBRECOMPRADOR")
    private String buyerName;
    @Column(name = "EMAILCOMPRADOR")
    private String buyerEmail;
    @Column(name = "DATOSADICIONALES")
    private String additionalData;
    @Column(name = "CODIGOERROR")
    private String errorCode;
    @Column(name = "MENSAJEERROR")
    private String errorMessage;
    @Column(name = "FRANQUICIA")
    private String franchise;
    @Column(name = "NUMEROAUTORIZACIONTX")
    private String authNumberTx;
    @Column(name = "NUMERORECIBO")
    private String receiptNumber;
    @Column(name = "FECHATRANSACCION")
    private LocalDateTime transactionDate;
    @Column(name = "NUMEROTARJETACREDITO")
    private String creditCardNumber;
    @Column(name = "NOMBREBANCO")
    private String bankName;
    @Column(name = "CODIGOERRORTA")
    private String errorCodeTa;
    @Column(name = "MENSAJEERRORTA")
    private String errorMessageTa;
    @Column(name = "NUMEROAUTORIZACIONTA")
    private String authNumberTa;
    @Column(name = "NUMERORECIBOTA")
    private String receiptNumberTa;
    @Column(name = "RESPUESTATRANSACCION")
    private String transactionResponse;
    @Column(name = "ESTADOTRANSACCION")
    private String transactionStatus;
    @Column(name = "NUMEROFACTURA")
    private Integer billNumber;
    @Column(name = "FECHAPAGO")
    private LocalDateTime payDate;
    @Column(name = "SUCURSAL")
    private String branchOffice;
    @Column(name = "CIUDADCOMPRADOR")
    private String buyerCity;
    @Column(name = "VALOR_BANCO")
    private Double bankValue;
    @Column(name = "VALOR_SALDO")
    private Double balanceValue;
    @Column(name = "DESCUENTO_CLIENTE")
    private Double buyerDiscount;
    @Column(name = "PORCENTAJE_DESCUENTO")
    private Integer percentageDiscount;
    @Column(name = "IDCOMPRADOR")
    private String buyerId;
    @Column(name = "VALOR_BANCO_OTRAS_LOTERIAS")
    private Double bankValueOtherLotteries;
    @Column(name = "VALOR_SALDO_OTRAS_LOTERIAS")
    private Double balanceValueOtherLotteries;
    @Column(name = "CUSTOMER_SITE_ID")
    private String buyerSiteId;
    @Column(name = "IDPASARELA")
    private String pasarelaId;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "lotterySale")
    private List<SaleHistoryRecord> saleHistory;
}
