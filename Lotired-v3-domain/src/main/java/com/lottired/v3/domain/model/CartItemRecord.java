package com.lottired.v3.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name= "CART_ITEM")
public class CartItemRecord extends EntityWithUUID{

    private String lotteryId;
    private String drawId;
    private String numberPlay;
    private double value;
    private String serieId;
    private String fraction;
    private String discount;
    private String coupon;
    private int reservationId;
    private int itemQuantity;
    private LocalDateTime createdAt;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cart_id")
    private CartRecord cart;

}
