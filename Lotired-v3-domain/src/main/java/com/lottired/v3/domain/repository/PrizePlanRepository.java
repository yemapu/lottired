package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.PrizePlanRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PrizePlanRepository extends JpaRepository<PrizePlanRecord, Long> {

    @Query(value = "SELECT * FROM lottery_schema.plandepremios WHERE loteria = :lotteryId AND sorteo = :drawCode", nativeQuery = true)
    List<PrizePlanRecord> findAllByQuery(@Param("lotteryId") String lotteryId, @Param("drawCode") String drawCode);
}
