package com.lottired.v3.domain.model;

import com.lottired.v3.domain.pk.DistributorPK;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "DISTRIBUIDORESLOTERIAS", schema = "lottery_schema")
public class LotteryDistributorRecord {
    @EmbeddedId
    private DistributorPK distributorPK;

    @Column(name = "BLOQUEADA")
    private String block;

    @Column(name = "VENTA_SALDO")
    private String saleBalance;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "empresa", referencedColumnName = "empresa", insertable = false, updatable = false),
            @JoinColumn(name = "cadena", referencedColumnName = "cadena", insertable = false, updatable = false),
            @JoinColumn(name = "loteria", referencedColumnName = "loteria", insertable = false, updatable = false)
    })
    private LotteryDistributorRecord lotteryDistributor;


}
