package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.IHistoricalResult;
import com.lottired.v3.domain.model.DrawRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DrawRepository extends JpaRepository<DrawRecord, Long> {

    @Query(value = "SELECT * FROM lottery_schema.SORTEOS sor INNER JOIN lottery_schema.LOTERIAS lot ON lot.LOTERIA=sor.LOTERIA AND lot.SORTEO=sor.SORTEO AND lot.ESTADO = '01' WHERE sor.LOTERIA IN (SELECT LOTERIA FROM lottery_schema.DISTRIBUIDORESLOTERIAS distLot WHERE distLot.cadena = '183' AND distLot.bloqueada = 'N')  ORDER BY sor.loteria DESC", nativeQuery = true)
    List<DrawRecord> getAllDraws();

    @Query(value = "SELECT pp.sorteo AS draw, pp.codigopremio AS prizeCode, pp.descripcion AS description, pp.valorpremio AS prizeValue, pp.valorpremiofraccion AS prizeFractionValue, pp.cantidadpremios AS prizeQuantity, r.numero AS number, r.serie AS serie, r.item AS item " +
            "FROM lottery_schema.plandepremios pp, lottery_schema.resultados r " +
            "WHERE pp.empresa = '01' AND pp.loteria = :lotteryId and pp.sorteo = :drawCode AND pp.aproximacion = 'N' " +
            "AND r.empresa = pp.empresa AND r.loteria = pp.loteria AND r.sorteo = pp.sorteo AND r.codigopremio = pp.codigopremio " +
            "ORDER BY pp.codigopremio, PP.numero", nativeQuery = true)
    List<IHistoricalResult> countHistoricalResult(@Param("lotteryId") String lotteryId, @Param("drawCode") String drawCode);
}
