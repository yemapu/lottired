package com.lottired.v3.domain.pk;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class DrawPK implements Serializable {
    @Column(name = "EMPRESA")
    private String enterprise;

    @Column(name = "LOTERIA")
    private String lottery;

    @Column(name = "SORTEO")
    private String draw;
}
