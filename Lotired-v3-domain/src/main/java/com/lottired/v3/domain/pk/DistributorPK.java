package com.lottired.v3.domain.pk;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class DistributorPK implements Serializable {

    @Column(name = "EMPRESA")
    private String enterprise;

    @Column(name = "CADENA")
    private String string;

    @Column(name = "LOTERIA")
    private String lottery;
}
