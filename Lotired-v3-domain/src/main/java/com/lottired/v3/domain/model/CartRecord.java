package com.lottired.v3.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "CART")
public class CartRecord extends EntityWithUUID {

    private String payMethod;
    private String sessionId;
    private String userId;
    private double totalCost;
    private LocalDateTime expirationDate;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "cart")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<CartItemRecord> items;
}
