package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.LotterySalesRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LotterySaleRepository extends JpaRepository<LotterySalesRecord, Long> {

    @Query(value = "SELECT * FROM lottery_schema.ventasloteriasportal WHERE ID = :saleId", nativeQuery = true)
    List<LotterySalesRecord> findAllByQuery(@Param("saleId") long saleId);

    @Query(value = "SELECT *,(SELECT vlp.ID from lottery_schema.Ventasloteriasportal vlp where vlp.NUMEROFACTURA = V.numerofactura AND vlp.ID != v.ID AND REFERENCIA IS NOT NULL AND VALORTOTAL != 0) AS IDASOCIADO FROM lottery_schema.ventasloteriasportal v WHERE v.numerofactura = :billId and v.id != :saleId AND v.REFERENCIA IS NOT NULL", nativeQuery = true)
    List<LotterySalesRecord> findAllByQueryOther(@Param("saleId") long saleId, @Param("billId") long billId);
}
