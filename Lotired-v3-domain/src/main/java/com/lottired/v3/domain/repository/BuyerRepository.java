package com.lottired.v3.domain.repository;

import com.lottired.v3.domain.model.BuyerRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BuyerRepository extends JpaRepository<BuyerRecord,Long> {
}
