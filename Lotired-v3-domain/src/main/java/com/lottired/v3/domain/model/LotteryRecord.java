package com.lottired.v3.domain.model;

import com.lottired.v3.domain.pk.LotteryPK;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "LOTERIAS", schema = "lottery_schema")
public class LotteryRecord {

    @EmbeddedId
    private LotteryPK id;

    @Column(name = "NOMBRE")
    private String name;

    @Column(name = "ABREVIATURA")
    private String abbreviation;

    @Column(name = "DIA")
    private int day;

    @Column(name = "HORAAPERTURA")
    private LocalDateTime openingTime;

    @Column(name = "HORACIERRE")
    private LocalDateTime closingTime;

    @Column(name = "ESTADO")
    private String status;

    @Column(name = "ULTIMONUMERO")
    private Long lastNumber;

    @Column(name = "SORTEO")
    private String draw;

    @Column(name = "TOTALFRACCIONES")
    private Long totalFractions;

    @Column(name = "VENDE_ABONADOS")
    private String sellSubscribers;

    @Column(name = "PARMARCHIVO")
    private String firstFile;

    @Column(name = "TIPOLOTERIA")
    private String lotteryType;

    @Column(name = "VENTAFRACCIONADA")
    private String fractionalSale;

    @Column(name = "AZARCONTRAVENTA")
    private String random;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "admin_lottery_id")
    private AdminLotteryRecord adminLottery;

}
