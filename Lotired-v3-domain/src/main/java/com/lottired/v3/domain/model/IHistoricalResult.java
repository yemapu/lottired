package com.lottired.v3.domain.model;


public interface IHistoricalResult {
    String getDraw();

    String getPrizeCode();

    String getDescription();

    double getPrizeValue();

    double getPrizeFractionValue();

    int getPrizeQuantity();

    String getNumber();

    String getSerie();

    String getItem();
}
