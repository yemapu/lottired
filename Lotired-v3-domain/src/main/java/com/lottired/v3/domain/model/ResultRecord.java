package com.lottired.v3.domain.model;

import com.lottired.v3.domain.pk.ResultPk;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "RESULTADOS", schema = "lottery_schema")
public class ResultRecord {

    @EmbeddedId
    private ResultPk id;

    @Column(name = "VALORPREMIO")
    private Integer prizeValue;

    @Column(name = "TOTALFRACCIONES")
    private Integer totalFractions;

    @Column(name = "VALORPREMIOFRACCION")
    private Integer valuePrizeFraction;

    @Column(name = "PROCEDIMIENTO")
    private String process;

    @Column(name = "NUMEROANEXO")
    private String annexNumber;

    @Column(name = "ITEMPADRE")
    private int principalItem;

    @Column(name = "CODIGOPREMIOPADRE")
    private String principalPrizeCode;

    @Column(name = "TIPOPREMIOPADRE")
    private String principalPrizeType;

    @Column(name = "NIT")
    private String nit;

    @Column(name = "SUCURSAL")
    private String branchOffice;

    @Column(name = "NOMBRE")
    private String name;

    @Column(name = "CIUDAD")
    private String city;

    @Column(name = "VENDIDAS")
    private String sold;

    @Column(name = "NOTIFICADO")
    private boolean notify;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "tipopremio", referencedColumnName = "tipopremio", insertable = false, updatable = false),
            @JoinColumn(name = "codigopremio", referencedColumnName = "codigopremio", insertable = false, updatable = false)
    })
    private PrizePlanRecord prizePlan;

}
