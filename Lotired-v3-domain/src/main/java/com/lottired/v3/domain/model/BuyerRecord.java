package com.lottired.v3.domain.model;

import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Table(name = "COMPRADORES" , schema = "lottery_schema")
@Entity
public class BuyerRecord {
    @UniqueElements
    @Column(name = "CEDULA")
    private String document;

    @Column(name = "NOMBRES")
    private String fullName;

    @Column(name = "APELLIDOS")
    private String lastName;

    @Column(name = "DIRECCION")
    private String address;

    @Column(name = "TELEFONO")
    private String phone;

    @Column(name = "CIUDAD")
    private String city;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "FECHANACIMIENTO")
    private LocalDateTime birthDate;

    @Column(name = "SEXO")
    private String gender;

    @Column(name = "DEPARTAMENTO")
    private String province;

    @Column(name = "ESTADO")
    private Integer status;

    @Column(name = "CODIGOBANCO")
    private String bankCode;

    @Column(name = "MANEJO_TNP")
    private String handlingTnp;

    @Column(name = "MANEJO_TP")
    private String handlingTp;

    @Column(name = "CELULAR")
    private String cellPhone;

    @Column(name = "NUMEROCUENTA")
    private String acountNumber;

    @Column(name = "CLAVE")
    private String password;

    @Column(name = "CODIGOACTIVACION")
    private String activationCode;

    @Column(name = "PADRE")
    private String primaryContact;

    @Column(name = "NOMBRE1")
    private String firstName;

    @Column(name = "NOMBRE2")
    private String secondName;

    @Column(name = "APELLIDO1")
    private String surName;

    @Column(name = "APELLIDO2")
    private String secondSurname;

    @Column(name = "DIRECCION_CORRESP")
    private String currentAddress;

    @Column(name = "TELEFONO_OFICINA")
    private String workPhone;

    @Column(name = "TELEFONO_MOVIL")
    private String phoneMobile;

    @Column(name = "ID_SESION")
    private String sesionId;

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "TIPO_CUENTA")
    private String acountType;

    @Column(name = "COMPRA_MAX_LOT_GRATUITA")
    private Long topFreePurchase;

    @Column(name = "NOTIFICACIONESSMS")
    private String smsNotification;

    @Column(name = "NOTIFICACIONESMAIL")
    private String emailNotification;

    @Column(name = "IMPRIMIRTIKECTNOPREMIADO")
    private String printWinningTicket;

    @Column(name = "USUARIO_CAMBIO")
    private String changeUser;

    @Column(name = "DIRECCIONIP_CAMBIO")
    private String changeIp;

    @Column(name = "FECHA_REGISTRO")
    private LocalDateTime register;

    @Column(name = "SESIONMOVIL")
    private String mobileSession;

    @Column(name = "NOTIFICACIONESPUBAPPA")
    private String pubNotification;


}
