package com.lottired.v3.domain.pk;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
public class PrizePlanPK implements Serializable {

    @Column(name = "TIPOPREMIO")
    private String prizeType;

    @Column(name = "CODIGOPREMIO")
    private String prizeCode;
}
