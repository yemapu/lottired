package com.lottired.v3.domain.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "MOVIMIENTOS_SALDOS", schema = "lottery_schema")
public class BalanceMovementRecord {

    @Id
    @Column(name = "ID")
    private long id;
    @Column(name = "VALOR_MOVIMIENTO")
    private double valueMovement;
    @Column(name = "FECHA_MOVIMIENTO")
    private LocalDateTime movementDate;
    @Column(name = "SALDO")
    private double balance;
    @Column(name = "CEDULA")
    private String document;
}
