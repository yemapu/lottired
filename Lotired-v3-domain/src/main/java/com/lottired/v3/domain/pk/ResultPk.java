package com.lottired.v3.domain.pk;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
@Data
public class ResultPk implements Serializable {
    @Column(name = "NUMERO")
    private String number;

    @Column(name = "SERIE")
    private String serie;

    @Column(name = "ITEM")
    private String item;
}
