package com.lottired.v3.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "HISTORIALVENTAS", schema = "lottery_schema")
public class SaleHistoryRecord {

    @Id
    @Column(name = "ID")
    private long id;
    @Column(name = "SERIE")
    private String serie;
    @Column(name = "NUMERO")
    private Integer number;
    @Column(name = "LOTERIA")
    private String lottery;
    @Column(name = "SORTEO")
    private String draw;
    @Column(name = "FRACCION")
    private Integer fraction;
    @Column(name = "VENTABRUTA")
    private Double grossSale;
    @Column(name = "SERIECOLILLA")
    private String serieColilla;
    @Column(name = "NUMEROCOLILLA")
    private Integer colillaNumber;
    @Column(name = "VALORFRACCION")
    private Double fractionValue;
    @Column(name = "CODIGO_BARRAS")
    private String barCode;
    @Column(name = "FECHA_JUEGA")
    private String gameDate;
    @Column(name = "NOMBRE_LOTERIA")
    private String lotteryName;
    @Column(name = "SORTEO_INICIAL")
    private String initialDraw;
    @Column(name = "SORTEO_FINAL")
    private String endDraw;
    @Column(name = "CANTIDAD_SORTEOS")
    private Integer drawQuantity;
    @Column(name = "IDABONADO")
    private Integer subscriberId;
    @Column(name = "ITEM")
    private Integer item;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idventasloteriasportal")
    @JsonIgnore
    private LotterySalesRecord lotterySale;
}
