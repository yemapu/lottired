package com.lottired.v3.core.service;

import com.lottired.v3.common.request.CreateCartItemRequest;
import com.lottired.v3.common.request.CreateCartRequest;
import com.lottired.v3.domain.model.CartItemRecord;
import com.lottired.v3.domain.model.CartRecord;
import com.lottired.v3.domain.repository.CartItemRepository;
import com.lottired.v3.domain.repository.CartRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CartItemService {
    private final CartItemRepository cartItemRepository;
    private final CartService cartService;


    public CartItemService(CartItemRepository cartItemRepository,CartService cartService) {
        this.cartItemRepository = cartItemRepository;
        this.cartService = cartService;

    }


    public CartItemRecord createItem(CreateCartItemRequest request) throws Exception {

        CartItemRecord cartItem = new CartItemRecord();
        Optional<CartRecord> cartRecord = cartService.findById(request.getCardId());

        cartItem.setLotteryId(request.getLotteryId());
        cartItem.setDrawId(request.getDrawId());
        cartItem.setNumberPlay(request.getNumberPlay());
        cartItem.setValue(request.getValue());
        cartItem.setSerieId(request.getSerieId());
        cartItem.setFraction(request.getFraction());
        cartItem.setDiscount(request.getDiscount());
        cartItem.setCoupon(request.getCoupon());
        cartItem.setReservationId(request.getReservationId());
        cartItem.setItemQuantity(request.getItemQuantity());
        cartItem.setCreatedAt(request.getCreatedAt());

        if (cartRecord.isEmpty()){

            List<CartItemRecord> cartItemRecords = new ArrayList<>();
            CreateCartRequest cartRequest = new CreateCartRequest();

            cartRequest.setTotalCost(12655);
            cartRequest.setPayMethod("");
            cartRequest.setUserId("");
            cartRequest.setExpirationDate(LocalDateTime.now());
            cartRequest.setCreatedAt(LocalDateTime.now());
            cartRequest.setUpdatedAt(LocalDateTime.now());
            cartRequest.setSessionId("");
            cartRequest.setItems(cartItemRecords);

            cartService.addToCart(cartRequest);
        }else {
            cartItem.setCart(cartRecord.get());
        }


        return cartItemRepository.save(cartItem);
    }

    public CartItemRecord findById(String cartItemId){
        return cartItemRepository.getById(UUID.fromString(cartItemId));
    }
    @Transactional
    public void deleteById(String cartItemId){
        cartItemRepository.deleteById(UUID.fromString(cartItemId));
    }

    @Transactional
    public void deleteAllByCartId(String cartId){
        cartItemRepository.deleteAllByCartId(UUID.fromString(cartId));
    }
}
