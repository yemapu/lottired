package com.lottired.v3.core.service;

import com.lottired.v3.domain.model.LotterySalesRecord;
import com.lottired.v3.domain.repository.LotterySaleRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LotterySalesService {

    private final LotterySaleRepository lotterySaleRepository;

    public LotterySalesService(LotterySaleRepository lotterySaleRepository) {
        this.lotterySaleRepository = lotterySaleRepository;
    }

    public List<LotterySalesRecord> getAllPaging(int page, int size){
        Pageable pageRequest = PageRequest.of(page, size);
        return lotterySaleRepository.findAll(pageRequest).getContent();
    }
}
