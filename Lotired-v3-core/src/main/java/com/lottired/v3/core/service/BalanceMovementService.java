package com.lottired.v3.core.service;

import com.lottired.v3.domain.model.BalanceMovementRecord;
import com.lottired.v3.domain.repository.BalanceMovementRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BalanceMovementService {

    private final BalanceMovementRepository balanceMovementRepository;

    public BalanceMovementService(BalanceMovementRepository balanceMovementRepository) {
        this.balanceMovementRepository = balanceMovementRepository;
    }


    public List<BalanceMovementRecord> getAllPaging(int page, int size){
        Pageable pageRequest = PageRequest.of(page, size);
        return balanceMovementRepository.findAll(pageRequest).getContent();
    }

}
