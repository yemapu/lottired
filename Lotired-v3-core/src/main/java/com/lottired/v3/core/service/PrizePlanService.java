package com.lottired.v3.core.service;

import com.lottired.v3.common.mapper.PrizePlanMapper;
import com.lottired.v3.common.response.PrizePlanResponse;
import com.lottired.v3.domain.model.PrizePlanRecord;
import com.lottired.v3.domain.repository.PrizePlanRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PrizePlanService {

    private final PrizePlanRepository prizePlanRepository;

    public PrizePlanService(PrizePlanRepository prizePlanRepository) {
        this.prizePlanRepository = prizePlanRepository;
    }


    public List<PrizePlanResponse> getAll(){
        return prizePlanRepository.findAll().stream().map(PrizePlanMapper::mapToPrizePlanResponse).collect(Collectors.toList());
    }

    public List<PrizePlanResponse> getAllByQuery(String lottery, String draw){
        return prizePlanRepository.findAllByQuery(lottery,draw).stream().map(PrizePlanMapper::mapToPrizePlanResponse).collect(Collectors.toList());
    }
}
