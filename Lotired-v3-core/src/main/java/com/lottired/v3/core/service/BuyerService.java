package com.lottired.v3.core.service;

import com.lottired.v3.common.mapper.BuyerMapper;
import com.lottired.v3.common.response.BuyerResponse;
import com.lottired.v3.domain.model.BuyerRecord;
import com.lottired.v3.domain.repository.BuyerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BuyerService {

    private final BuyerRepository buyerRepository;

    public BuyerService(BuyerRepository buyerRepository) {
        this.buyerRepository = buyerRepository;
    }

    public List<BuyerResponse> getAll(int page, int size){
        Pageable pageRequest = PageRequest.of(page,size);
        return buyerRepository.findAll(pageRequest).getContent().stream().map(BuyerMapper::mapToBuyerResponse).collect(Collectors.toList());
    }
}
