package com.lottired.v3.core.service;

import com.lottired.v3.common.request.AddItemsRequest;
import com.lottired.v3.common.request.CreateCartRequest;
import com.lottired.v3.domain.model.CartItemRecord;
import com.lottired.v3.domain.model.CartRecord;
import com.lottired.v3.domain.repository.CartRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CartService {

    private final CartRepository cartRepository;


    public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;

    }


    public List<CartRecord> getAll(){
        return cartRepository.findAll();
    }

    public CartRecord addToCart(CreateCartRequest request){

        CartRecord cart = new CartRecord();

        cart.setTotalCost(request.getTotalCost());
        cart.setPayMethod(request.getPayMethod());
        cart.setUserId(request.getUserId());
        cart.setExpirationDate(request.getExpirationDate());
        cart.setCreatedAt(request.getCreatedAt());
        cart.setUpdatedAt(request.getUpdatedAt());
        cart.setSessionId(request.getSessionId());
        cart.setItems(request.getItems());

        return cartRepository.save(cart);
    }

    @Transactional
    public void deleteById(String cartId){
        cartRepository.deleteById(UUID.fromString(cartId));
    }

    public Optional<CartRecord> findById(String id){
        return cartRepository.findById(UUID.fromString(id));
    }

}
