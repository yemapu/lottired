package com.lottired.v3.core.service;

import com.lottired.v3.domain.model.IHistoricalResult;
import com.lottired.v3.domain.model.LotteryRecord;
import com.lottired.v3.domain.model.DrawRecord;
import com.lottired.v3.domain.repository.LotteryRepository;
import com.lottired.v3.domain.repository.DrawRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LotteryService {

    private final LotteryRepository lotteryRepository;
    private final DrawRepository drawRepository;


    public LotteryService(LotteryRepository lotteryRepository, DrawRepository drawRepository) {
        this.lotteryRepository = lotteryRepository;
        this.drawRepository = drawRepository;
    }

    public List<LotteryRecord> getAll() {
        return lotteryRepository.findAll();
    }

    public List<DrawRecord> getAllSorteos() {
        return drawRepository.findAll();
    }

    public List<DrawRecord> findAllByStatus() {
        return drawRepository.getAllDraws();
    }

    public List<IHistoricalResult> getAllHistorical(String lotteryId, String drawCode) {
        return drawRepository.countHistoricalResult(lotteryId, drawCode);
    }
}
