package com.lottired.v3.core.service;

import com.lottired.v3.common.mapper.DrawMapper;
import com.lottired.v3.common.response.DrawResponse;
import com.lottired.v3.domain.repository.DrawRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DrawService {
    private final DrawRepository drawRepository;

    public DrawService(DrawRepository drawRepository) {
        this.drawRepository = drawRepository;
    }

    public List<DrawResponse> getAll(){
        return drawRepository.getAllDraws().stream().map(DrawMapper::mapToLotteryResponse).collect(Collectors.toList());
    }
}
