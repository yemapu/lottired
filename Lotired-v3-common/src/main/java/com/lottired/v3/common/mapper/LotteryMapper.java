package com.lottired.v3.common.mapper;

import com.lottired.v3.common.response.LotteryResponse;
import com.lottired.v3.domain.model.LotteryRecord;

public class LotteryMapper {

    public static LotteryResponse mapToLotteryResponse(LotteryRecord lotteryRecord) {
        LotteryResponse lotteryResponse = new LotteryResponse();
        lotteryResponse.setName(lotteryRecord.getName());
        lotteryResponse.setHoraApertura(lotteryRecord.getOpeningTime());

        return lotteryResponse;

    }
}
