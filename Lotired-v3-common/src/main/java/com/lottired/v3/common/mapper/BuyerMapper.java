package com.lottired.v3.common.mapper;

import com.lottired.v3.common.response.BuyerResponse;
import com.lottired.v3.domain.model.BuyerRecord;

public class BuyerMapper {

    public static BuyerResponse mapToBuyerResponse(BuyerRecord buyerRecord) {
        BuyerResponse buyerResponse = new BuyerResponse();

        buyerResponse.setBirthDate(buyerRecord.getBirthDate());
        buyerResponse.setGender(buyerRecord.getGender());
        buyerResponse.setEmail(buyerRecord.getEmail());
        buyerResponse.setPhone(buyerRecord.getPhone());
        buyerResponse.setCellPhone(buyerRecord.getCellPhone());
        buyerResponse.setProvince(buyerRecord.getProvince());
        buyerResponse.setCity(buyerRecord.getCity());
        buyerResponse.setAddress(buyerRecord.getAddress());
        buyerResponse.setFirstName(buyerRecord.getFirstName());
        buyerResponse.setSecondName(buyerRecord.getSecondName());
        buyerResponse.setSurName(buyerRecord.getSurName());
        buyerResponse.setSecondSurname(buyerRecord.getSecondSurname());
        buyerResponse.setDocument(buyerRecord.getDocument());

        return buyerResponse;

    }
}
