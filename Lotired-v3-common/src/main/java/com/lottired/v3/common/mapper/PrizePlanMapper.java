package com.lottired.v3.common.mapper;

import com.lottired.v3.common.response.PrizePlanResponse;
import com.lottired.v3.domain.model.PrizePlanRecord;

public class PrizePlanMapper {

    public static PrizePlanResponse mapToPrizePlanResponse(PrizePlanRecord prizePlan) {
        PrizePlanResponse prizePlanResponse = new PrizePlanResponse();
        prizePlanResponse.setDescription(prizePlan.getDescription());
        prizePlanResponse.setPrizeValue(prizePlan.getPrizeValue());
        prizePlanResponse.setTotalFractions(prizePlan.getTotalFractions());
        prizePlanResponse.setValuePrizeFraction(prizePlan.getValuePrizeFraction());
        prizePlanResponse.setProcess(prizePlan.getProcess());
        prizePlanResponse.setPrizeQuantity(prizePlan.getPrizeQuantity());
        prizePlanResponse.setNumber(prizePlan.getNumber());
        prizePlanResponse.setPrimaryPrize(prizePlan.getPrimaryPrize());
        prizePlanResponse.setDrawCode(prizePlan.getDraw().getDrawPK().getDraw());
        System.out.println(prizePlan.getDraw().getDrawPK().toString());
        return prizePlanResponse;

    }
}

