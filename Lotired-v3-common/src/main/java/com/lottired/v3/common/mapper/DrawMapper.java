package com.lottired.v3.common.mapper;

import com.lottired.v3.common.response.DrawResponse;
import com.lottired.v3.domain.model.AdminLotteryRecord;
import com.lottired.v3.domain.model.DrawRecord;

import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Optional;

public class DrawMapper {

    public static DrawResponse mapToLotteryResponse(DrawRecord drawRecord) {
        DrawResponse drawResponse = new DrawResponse();
        LocalDateTime drawDate = LocalDateTime.of(drawRecord.getStartAt().toLocalDate(), drawRecord.getCloseHour().toLocalTime());
        Optional<AdminLotteryRecord> adminLotteryRecord = Optional.ofNullable(drawRecord.getLotteryRecord().getAdminLottery());
        adminLotteryRecord.ifPresent(adminLottery -> {
            drawResponse.setLotteryLog(adminLottery.getLogo());
           drawResponse.setLotteryDisplayName(Optional.ofNullable(adminLottery.getDisplayName()).orElse(drawRecord.getLotteryRecord().getName()));

        });
        drawResponse.setLotteryId(drawRecord.getLotteryRecord().getId().getLottery());
        drawResponse.setCode(drawRecord.getDrawPK().getDraw());
        drawResponse.setGameDate(drawDate);
        drawResponse.setGameDay(drawRecord.getStartAt().getDayOfWeek().getDisplayName(TextStyle.FULL,
                Locale.getDefault()));
        drawResponse.setJackPot(drawRecord.getGreatestPrizeValue());
        drawResponse.setLotteryName(drawRecord.getLotteryRecord().getName());
        drawResponse.setClosed(LocalDateTime.now().isAfter(drawDate));
        drawResponse.setTicketValue(drawRecord.getTicketValue());
        drawResponse.setTicketFractionValue(drawRecord.getTicketFractionValue());
        drawResponse.setTotalFractions(drawRecord.getTotalFractions());
        return drawResponse;

    }
}
