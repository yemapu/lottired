package com.lottired.v3.common.request;

import lombok.Data;

import java.util.List;

@Data
public class AddItemsRequest {

    private String cartId;
    private List<String> cartItemId;
}
