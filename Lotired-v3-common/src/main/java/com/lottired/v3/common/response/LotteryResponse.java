package com.lottired.v3.common.response;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LotteryResponse {

    private String name;
    private LocalDateTime horaApertura;
}
