package com.lottired.v3.common.request;

import lombok.Data;
import java.time.LocalDateTime;

@Data
public class CreateCartItemRequest{
    private String lotteryId;
    private String drawId;
    private String numberPlay;
    private double value;
    private String serieId;
    private String fraction;
    private String discount;
    private String coupon;
    private int reservationId;
    private int itemQuantity;
    private LocalDateTime createdAt;
    private String cardId;
}
