package com.lottired.v3.common.request;

import com.lottired.v3.domain.model.CartItemRecord;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class CreateCartRequest {

    private String payMethod;
    private String sessionId;
    private String userId;
    private double totalCost;
    private LocalDateTime expirationDate;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private List<CartItemRecord> items;
}
