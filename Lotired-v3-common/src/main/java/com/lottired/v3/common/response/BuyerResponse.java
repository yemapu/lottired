package com.lottired.v3.common.response;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BuyerResponse {

    private LocalDateTime birthDate;
    private String gender;
    private String email;
    private String phone;
    private String cellPhone;
    private String province;
    private String city;
    private String address;
    private String firstName;
    private String secondName;
    private String surName;
    private String secondSurname;
    private String document;


}
