package com.lottired.v3.common.response;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DrawResponse {

    private String lotteryId;
    private String lotteryLog;
    private String lotteryName;
    private String lotteryDisplayName;
    private String code;
    private LocalDateTime gameDate;
    private String gameDay;
    private double jackPot;
    private boolean closed;
    private String ticketValue;
    private double ticketFractionValue;
    private long totalFractions;
}
