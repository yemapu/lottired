package com.lottired.v3.common.response;

import lombok.Data;

@Data
public class PrizePlanResponse {

    private String description;
    private double prizeValue;
    private int totalFractions;
    private double valuePrizeFraction;
    private String process;
    private int prizeQuantity;
    private String number;
    private String primaryPrize;
    private String drawCode;
}
